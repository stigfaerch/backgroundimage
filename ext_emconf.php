<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "backgroundimage".
 *
 * Auto generated 12-11-2018 15:25
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Background Image',
  'description' => 'Enables uploading of an image in page properties, which then is shown on page as background image for a predefined element.',
  'category' => 'fe',
  'version' => '4.1.0',
  'state' => 'stable',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearcacheonload' => true,
  'author' => 'Sven Burkert',
  'author_email' => 'bedienung@sbtheke.de',
  'author_company' => 'SBTheke web development',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '9.5.0-10.4.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

