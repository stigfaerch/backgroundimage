<?php
defined('TYPO3_MODE') || die();

$ll = 'LLL:EXT:backgroundimage/Resources/Private/Language/locallang.xlf:';

// Extra fields for the pages table
$newPagesColumns = [
    'tx_backgroundimage_active' => [
        'exclude' => 1,
        'l10n_mode' => 'exclude',
        'label' => $ll . 'pages.tx_backgroundimage_active',
        'requestUpdate' => 1,
        'config' => [
            'type' => 'check',
        ],
    ],
    'tx_backgroundimage_image' => [
        'exclude' => 1,
        'l10n_mode' => 'exclude',
        'label' => $ll . 'pages.tx_backgroundimage_image',
        'displayCond' => 'FIELD:tx_backgroundimage_active:REQ:true',
        'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
            'tx_backgroundimage_image',
            [
                'maxitems' => 1,
                'foreign_match_fields' => [
                    'fieldname' => 'tx_backgroundimage',
                ],
            ],
            $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
        ),
    ],
    'tx_backgroundimage_repeat' => [
        'exclude' => 1,
        'l10n_mode' => 'exclude',
        'label' => $ll . 'pages.tx_backgroundimage_repeat',
        'displayCond' => 'FIELD:tx_backgroundimage_active:REQ:true',
        'config' => [
            'type' => 'select',
            'items' => [
                [
                    $ll . 'pages.tx_backgroundimage_default',
                    ''
                ],
                [
                    $ll . 'pages.tx_backgroundimage_repeat.repeat',
                    'repeat'
                ],
                [
                    $ll . 'pages.tx_backgroundimage_repeat.norepeat',
                    'no-repeat'
                ],
                [
                    $ll . 'pages.tx_backgroundimage_repeat.repeatx',
                    'repeat-x'
                ],
                [
                    $ll . 'pages.tx_backgroundimage_repeat.repeaty',
                    'repeat-y'
                ],
            ],
        ],
    ],
    'tx_backgroundimage_color' => [
        'exclude' => 1,
        'l10n_mode' => 'exclude',
        'label' => $ll . 'pages.tx_backgroundimage_color',
        'displayCond' => 'FIELD:tx_backgroundimage_active:REQ:true',
        'config' => [
            'type' => 'input',
            'max' => 20,
            'eval' => 'trim',
            'wizards' => [
                'colorChoice' => [
                    'type' => 'colorbox',
                    'module' => [
                        'name' => 'wizard_colorpicker',
                    ],
                    'JSopenParams' => 'height=500,width=500,status=0,menubar=0,scrollbars=1',
                ],
            ],
        ],
    ],
    'tx_backgroundimage_position' => [
        'exclude' => 1,
        'l10n_mode' => 'exclude',
        'label' => $ll . 'pages.tx_backgroundimage_position',
        'displayCond' => 'FIELD:tx_backgroundimage_active:REQ:true',
        'config' => [
            'type' => 'input',
            'max' => 20,
            'eval' => 'trim',
        ],
    ],
    'tx_backgroundimage_size' => [
        'exclude' => 1,
        'l10n_mode' => 'exclude',
        'label' => $ll . 'pages.tx_backgroundimage_size',
        'displayCond' => 'FIELD:tx_backgroundimage_active:REQ:true',
        'config' => [
            'type' => 'input',
            'max' => 20,
            'eval' => 'trim',
        ],
    ],
    'tx_backgroundimage_attachment' => [
        'exclude' => 1,
        'l10n_mode' => 'exclude',
        'label' => $ll . 'pages.tx_backgroundimage_attachment',
        'displayCond' => 'FIELD:tx_backgroundimage_active:REQ:true',
        'config' => [
            'type' => 'select',
            'items' => [
                [
                    $ll . 'pages.tx_backgroundimage_default',
                    ''
                ],
                [
                    $ll . 'pages.tx_backgroundimage_attachment.scroll',
                    'scroll'
                ],
                [
                    $ll . 'pages.tx_backgroundimage_attachment.fixed',
                    'fixed'
                ],
                [
                    $ll . 'pages.tx_backgroundimage_attachment.local',
                    'local'
                ],
            ],
        ],
    ],
];

// Adding fields to the pages table definition in TCA
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $newPagesColumns);

// Create palette
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages', '--palette--;LLL:EXT:backgroundimage/Resources/Private/Language/locallang.xlf:pages.palette.backgroundimage;backgroundimage', '', 'after:media');
$GLOBALS['TCA']['pages']['palettes']['backgroundimage']['showitem'] = 'tx_backgroundimage_active, --linebreak--, tx_backgroundimage_image, --linebreak--, tx_backgroundimage_repeat, tx_backgroundimage_color, tx_backgroundimage_position, tx_backgroundimage_size, tx_backgroundimage_attachment';

// Request an update
$GLOBALS['TCA']['pages']['ctrl']['requestUpdate'] .= (empty($GLOBALS['TCA']['pages']['ctrl']['requestUpdate']) === TRUE ? '' : ',') . 'tx_backgroundimage_active';