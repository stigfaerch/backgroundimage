page.cssInline.440 = COA
page.cssInline.440 {
    if.isTrue.data = levelfield:-1, tx_backgroundimage_active
    stdWrap {
        required = 1
        wrap = {$plugin.backgroundimage.target}{|}
    }
    10 = FILES
    10 {
        stdWrap {
            required = 1
            wrap = background-image:url(/|);
        }
        references {
            table = pages
            uid.data = page:uid
            fieldName = tx_backgroundimage
            #data = levelfield:-1, tx_backgroundimage_image // TODO: write just this line when dropping TYPO3 8.7 support
        }
        renderObj = IMG_RESOURCE
        renderObj {
            file.import.dataWrap = {file:current:storage}:{file:current:identifier}
        }
    }
    20 = TEXT
    20 {
        required = 1
        wrap = background-repeat:|;
        data = levelfield:-1, tx_backgroundimage_repeat
    }
    30 = TEXT
    30 {
        required = 1
        wrap = background-color:|;
        data = levelfield:-1, tx_backgroundimage_color
    }
    40 = TEXT
    40 {
        required = 1
        wrap = background-position:|;
        data = levelfield:-1, tx_backgroundimage_position
    }
    50 = TEXT
    50 {
        required = 1
        wrap = background-size:|;
        data = levelfield:-1, tx_backgroundimage_size
    }
    60 = TEXT
    60 {
        required = 1
        wrap = background-attachment:|;
        data = levelfield:-1, tx_backgroundimage_attachment
    }
}

[{$plugin.backgroundimage.slide} == '1']
page.cssInline.440 {
    if.isTrue.data = levelfield:-1, tx_backgroundimage_active, slide
    10.references >
    10.references.data = levelfield:-1, tx_backgroundimage_image, slide
    20.data = levelfield:-1, tx_backgroundimage_repeat, slide
    30.data = levelfield:-1, tx_backgroundimage_color, slide
    40.data = levelfield:-1, tx_backgroundimage_position, slide
    50.data = levelfield:-1, tx_backgroundimage_size, slide
    60.data = levelfield:-1, tx_backgroundimage_attachment, slide
}
[end]