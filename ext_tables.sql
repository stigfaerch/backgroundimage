#
# Table structure for table "pages"
#
CREATE TABLE pages (
    tx_backgroundimage_active tinyint(1) DEFAULT '0' NOT NULL,
    tx_backgroundimage_image int(11) unsigned DEFAULT '0' NOT NULL,
    tx_backgroundimage_repeat varchar(10) DEFAULT '' NOT NULL,
    tx_backgroundimage_color varchar(20) DEFAULT '' NOT NULL,
    tx_backgroundimage_position varchar(20) DEFAULT '' NOT NULL,
    tx_backgroundimage_size varchar(20) DEFAULT '' NOT NULL,
    tx_backgroundimage_attachment varchar(10) DEFAULT '' NOT NULL,
);